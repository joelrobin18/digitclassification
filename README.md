# DigitClassification

## Introduction

In the currentage of digitalization, handwriting recognition plays an important role in information processing. Information is readily available on paper, and digital files require less processing than the processing of traditional paper files. The purpose of the handwriting recognition is to accurately recognize  handwriting  of  individuals  and  to  reduce  errors.

Detection  of  handwritten  numbers,  including  accuracy  in  these  areas,  has  reached  human perfectionusing deep convolutional neural networks (CNNs). Recently CNN has become one of the mostattractive approaches  and has been the ultimate factor in recent success  and in several challengingmachine learning applications.

# Problem Statement

Create a Deep Learning Model which could predict handwritten numbers from 0-9 with an accuracy of more than 97%.

## Background

The libraries Sklearn, Pytorch, Numpy, Matplotlib, Path, Torchvision, Torchmetrics, and Mlxtend are used. Many transforms, including ToTensor, CenterCrop, Normalize, and dataset splitting with DataLoader,  are  utilized  to    preprocess  the  MNIST  dataset.  The  optimizers  used  are  SGD (Stochastic Gradient Descent) and Adam, and the loss functions used are CrossEntropyLoss and NLLLoss.  Gradient  Descent  and  Backpropagation  are  some  of  the  algorithms  employed.  ReLU and Softmax are the activation functions in use. The model developed by us is a model withLinear and Non-Linear Activation Functions (ReLU and SoftMax). Confusion Matrix and Accuracy are the two evaluation techniques employed to evaluate different models.

## Installation

pip install -r requirements.txt

Run the jupyter notebook

## Conclusion

We  developed  a  nonlinear  model  with  activation  layers  such  as  Linear, Flatten,  SoftMax  and ReLU. We used the MNIST dataset to train and test our model. The training loop took about 416 seconds to execute using an NVIDIA GPU, and we achieved a training accuracy of 99.71% and a testing accuracy of 97.53%. We used a learning rate of 0.003. After that, we changed the epoch to 35 with a learning rate of 0.00001 and increased our accuracy to 97.69%. We then ran the model for 100 epochs, which took about 2203 seconds to run using the CrossEntropyLoss loss function, and  improved  our accuracy  to  97.75%.  We  then  compared  our  model  with  existing  models  in sklearn and found that our model performed the best
