import torch
import torch.nn as nn
import torchvision
from torchvision.datasets import MNIST
from torch.utils.data import Dataset,DataLoader,Subset,ConcatDataset
import torchvision.transforms as tt
from torchvision.transforms import ToTensor,Compose
from torchmetrics import Accuracy,ConfusionMatrix,Precision,F1Score,Recall
from mlxtend.plotting import plot_confusion_matrix
import matplotlib.pyplot as plt
import random
import time

device = "cuda" if torch.cuda.is_available() else "cpu"

def get_device_current():
    """
    This functions return whether the device is GPU or CPU.
    Return cuda if its running in GPU and cpu if its running in CPU
    """
    if torch.cuda.is_available():
        return torch.device("cuda")
    else:
        return torch.device("cpu")
    
def to_device(data, device):
    """
    Converts the data into given device. 
    Takes the data and device as argument 
    and then converts the given data into given device
    """
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)

class DeviceDataLoader():
    """
    Converts the whole dataset or model or anything that is wrapped 
    inside this function. It calls the to_device function and then
    converts every data into the given device
    """
    def __init__(self,dataloader,device):
        self.dataloader = dataloader
        self.device = device
    
    def __iter__(self):
        
        for i in self.dataloader:
            yield to_device(i,self.device)
            
    def __len__(self):
        return len(self.dataloader)

def train_accuracy(model:nn.Module,train_data,digits):
    """
    Returns the training accuracy of the mode.
    Takes three arguments. Model, training dataset and the label.
    After calculating the accuracy of the model, it return the accuracy
    """
    pred_array=[]
    label=[]
    for (img,label_value) in train_data:
        model.to("cpu")
        pred_logits=model(img)
        pred_label=pred_logits.softmax(dim=1).argmax(dim=1)
        pred_array.append(pred_label)
        label.append(label_value)

    # Make the prediction array into tensor
    pred_array=torch.cat(pred_array)
    label=torch.tensor(label)

    accuracy=Accuracy(task="multiclass",
                        num_classes=len(digits))

    accuracyValue=accuracy(pred_array,label).item()
    model.to(device)
    return (accuracyValue*100)
    # print("Train Acuracy Value: ",accuracyValue*100)

def test_accuracy(model:nn.Module,test_data,digits):
    """
    Returns the testing accuracy of the mode.
    Takes three arguments. Model, testing dataset and the label.
    After calculating the accuracy of the model, it return the accuracy
    """
    pred_array=[]
    label=[]
    for (img,label_value) in test_data:
        model.to("cpu")
        pred_logits=model(img)
        pred_label=pred_logits.softmax(dim=1).argmax(dim=1)
        pred_array.append(pred_label)
        label.append(label_value)
        
    # Make the prediction array into tensor
    pred_array=torch.cat(pred_array)
    label=torch.tensor(label)

    accuracy=Accuracy(task="multiclass",
                        num_classes=len(digits))

    accuracyValue=accuracy(pred_array,label).item()
    model.to(device)
    return (accuracyValue*100)
    # print("Test Accuracy Value: ",accuracyValue*100)

# Loss Function
def lossFunction(lr:int,model):
    """
    Return an array which contains loss functions and optimizer.
    Takes two argument. learning rate and the model.
    It then return an array
    """
    nllloss=nn.NLLLoss()
    crossentropy=nn.CrossEntropyLoss()
    sgdoptim=torch.optim.SGD(params=model.parameters(),
                            lr=lr, momentum=0.9)
    adamoptim=torch.optim.Adam(params=model.parameters(),lr=lr)
    
    return nllloss,crossentropy,sgdoptim,adamoptim

# Random Prediction
def random_pred(model:nn.Module,test_data):
    """
    Shows random prediction. Takes two arguments.
    Model and testing dataset
    """
    r=6
    c=4
    model.to("cpu")
    plt.figure(figsize=(15,17))
    for i in range(r*c):
        random_number=int(torch.rand(1)*10000)
        image1,label1=test_data[random_number]

        pred_test=model(image1)
        pred_test=pred_test.softmax(dim=1).argmax(dim=1).item()
        
        plt.subplot(r,c,i+1)
        if pred_test==label1:
            plt.title(f"Actual: {label1}|| Predicted: {pred_test}",c='g')
        else:
            plt.title(f"Actual: {label1}|| Predicted: {pred_test}",c='r')
        plt.imshow(image1.squeeze(),cmap="gray")
        plt.axis(False)

# Confusion Metrix
def confusion_matrix(model:nn.Module,test_data,digits):
    """
    Plots the confusion matrix. Takes three inputs.
    Model, testing dataset and the label
    """
    model.to("cpu")
    confMatrix=ConfusionMatrix(task="multiclass",num_classes=len(digits))

    pred_array=[]
    label=[]
    for (img,label_value) in test_data:
        pred_logits=model(img)
        pred_label=pred_logits.softmax(dim=1).argmax(dim=1)
        pred_array.append(pred_label)
        label.append(label_value)

    # Make the prediction array into tensor
    pred_array=torch.cat(pred_array)
    label=torch.tensor(label)

    pred_array[:10],label[:10]

    class_label=['0','1','2','3','4','5','6','7','8',9]
    predMatrix=confMatrix(preds=pred_array,
                            target=label)

    fig,ax=plot_confusion_matrix(
        conf_mat=predMatrix.numpy(),
        class_names=class_label,
        figsize=(8,8)
    )

def loss_plotting(train_loss,test_loss):
    """
    Plots the curve between the training loss and testing loss.
    Takes two inputs. Both are losses of training and testing respectively.
    """
    plt.figure()
    plt.plot(range(len(train_loss)),torch.tensor(train_loss).cpu().numpy(),label='Training Loss')
    plt.plot(range(len(test_loss)),torch.tensor(test_loss).cpu().numpy(),label='Test Loss')
    plt.legend()
    plt.xlabel('Iterations')
    plt.ylabel('Loss')
    plt.title('Training and Test Loss over Iterations')
    plt.show()

def images(train_data):
    """
    Shows random images from the dataset. Along with its label.
    Takes only input whose images we need to show
    """
    r=4
    c=9
    plt.figure(figsize=(15,8))
    for i in range(r*c):
        plt.subplot(r,c,i+1)
        img,label=train_data[i]
        plt.title(f"Digit: {label}")
        plt.imshow(img.squeeze(),cmap="gray")
        plt.axis(False)